<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>
<h1>
  Thank you for playing <i><?php echo CHtml::encode(Yii::app()->name); ?></i>
</h1>
<p>Your answer has been received and will be manually evaluated.</p>