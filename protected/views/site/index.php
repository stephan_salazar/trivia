<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>
<h1>
  Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i>
</h1>
<p>The trivia (singular trivium) are three lower Artes Liberales, i.e. grammar,
  logic and rhetoric. These were the topics of basic education, foundational to
  the quadrivia of higher education, and hence the material of basic education
  and an important building block for all undergraduates. The word trivia was
  also used to describe a place where three roads met in Ancient Rome. While the
  term is now obsolescent, in ancient times, it was appropriated to mean
  something very new.
</p> <p> 
   In the 1960s, nostalgic college students and others began
  to informally trade questions and answers about the popular culture of their
  youth. The first known documented labeling of this casual parlor game as
  "Trivia" was in a Columbia Daily Spectator column published on February 5,
  1965.[citation needed] The authors, Ed Goodgold and Dan Carlinsky, then
  started the first organized trivia contests, described below. Since the 1960s,
  the plural trivia in particular has widened to include nonessential,
  specifically detailed knowledge on topics of popular culture. The expression
  has also come to suggest information of the kind useful almost exclusively for
  answering quiz questions, hence the brand name Trivial Pursuit (1982).
</p>
<p class='right'> - Wikipedia, <a href='http://en.wikipedia.org/wiki/Trivia'>http://en.wikipedia.org/wiki/Trivia</a> - </p>