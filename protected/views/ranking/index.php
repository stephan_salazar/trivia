<?php
/* @var $this AnswersController */
/* @var $dataProvider CActiveDataProvider */


?>

<h1>Ranking</h1>


<table>
<tr>
  <th>Position</th>
  <th>Name</th>
  <th>Submission date and time</th>
</tr>
<?php 
$position = 1;
foreach ($model as $answer){
    echo "<tr>\n";
    echo "\t<td>".$position."</td>\n";
	echo "\t<td>".$answer->attributes['full_name']."</td>\n";
	echo "\t<td>".$answer->attributes['created']."</td>\n";
    echo "</tr>\n";
    
    $position++;
}
echo "</table>";

?>