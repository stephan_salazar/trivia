<?php
/* @var $this AnswersController */
/* @var $model Answers */

$this->breadcrumbs=array(
	'Answers'=>array('index'),
	$model->answer_id=>array('view','id'=>$model->answer_id),
	'Update',
);

$this->menu=array(
	array('label'=>'Manage Answers', 'url'=>array('admin')),
);
?>

<h1>Evaluate answer from "<?php echo $model->full_name; ?>" to question #<?php echo $model->question_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>