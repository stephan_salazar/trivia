<?php
/* @var $this PlayController */
/* @var $model Answers */

$this->breadcrumbs=array(
	'Answers'=>array('index'),
	'Create',
);

$this->menu=array(
// 	array('label'=>'List Answers', 'url'=>array('index')),
// 	array('label'=>'Manage Answers', 'url'=>array('admin')),
);
?>

<h1>Question</h1>
<p>
<?php echo $this->questionText?>
</p>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>