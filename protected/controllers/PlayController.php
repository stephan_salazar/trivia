<?php

class PlayController extends Controller {
  /**
   *
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   *      using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout = '//layouts/column2';
  public $questionText;
  /**
   *
   * @return array action filters
   */
  public function filters() {
    return array ();
  }
  
  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   *
   * @return array access control rules
   */
  public function accessRules() {
    return array (
        array (
            'allow', // allow authenticated user to perform 'create' and 'update' actions
            'actions' => array (
                'create' 
            ),
            'users' => array (
                '*' 
            ) 
        ),
        array (
            'deny', // deny all users
            'users' => array (
                '*' 
            ) 
        ) 
    );
  }
  
  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate() {
    
    $question_model = new Questions();
    $sql = "SELECT * FROM questions WHERE active = 1";
    $question = $question_model->findBySql($sql);
    
    $model = new Answers();
    
    $model->setAttribute('question_id', $question->getAttribute('question_id'));
    $this->questionText = $question->getAttribute('question');
    
    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);
    
    if (isset( $_POST ['Answers'] )) {
      $model->attributes = $_POST ['Answers'];
      if ($model->save()) $this->redirect( array (
          'site/thanks',
      ) );
    }
    
    $this->render( 'create', array (
        'model' => $model 
    ) );
  }

}
