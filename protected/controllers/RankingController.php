<?php

class RankingController extends Controller {
  public function actionIndex() {
    
    $sql = "SELECT * FROM answers NATURAL LEFT JOIN questions WHERE correct > 0 AND active > 0 ORDER BY created";
    $model = new Answers();
    $model = $model->findAllBySql($sql);
        
    $this->render('index',array(
			'model'=>$model,
		));
    
  }

}